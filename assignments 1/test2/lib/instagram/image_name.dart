abstract class ImageName {
  static const avatar = 'assets/avatar.png';
  static const cat1 = 'assets/1.png';
  static const cat2 = 'assets/2.jpg';
  static const cat3 = 'assets/3.jpg';
  static const cat4 = 'assets/4.jpg';
  static const cat5 = 'assets/5.jpg';
  static const cat6 = 'assets/6.jpg';
}